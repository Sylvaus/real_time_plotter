import os
import re
import codecs
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    with codecs.open(os.path.join(here, *parts), 'r') as fp:
        return fp.read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(
    name='real_time_plotter',
    version=find_version("real_time_plotter"),
    packages=[],
    url='',
    license='MIT',
    author='sylvaus',
    author_email='',
    description='Package providing helper classes to plot data in real time',
    install_requires=['numpy', 'pyserial', 'matplotlib'],
    test_suite="nose.collector",
    tests_requires=['coverage', 'nose'],
)
