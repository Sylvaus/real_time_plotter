from abc import ABC, abstractmethod
from typing import List, Union, Tuple


class ABSaver(ABC):
    def __init__(self, filename: str):
        self._filename = filename

    @abstractmethod
    def add_data_group(self, data: List[Tuple[float, List[Tuple[str, Union[int, float]]]]]):
        pass

    @abstractmethod
    def save(self):
        pass
