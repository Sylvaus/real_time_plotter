import struct
from typing import Union, Optional

from real_time_plotter.serializations import constants


class Serializer:
    def __init__(self, group_index: int):
        self._group_index = group_index
        self._body = bytes()

    def add_parameter(self, param_index: int, param_value: Union[int, float],
                      param_type: Optional[int] = None):

        param_bytes = struct.pack(constants.PARAM_INDEX_PACK_TYPE, param_index)
        if param_type is None:
            param_type = self.get_param_type(param_value)

        param_bytes += struct.pack(constants.PARAM_TYPE_PACK_TYPE, param_type)
        param_bytes += struct.pack(constants.PACK_TYPE[param_type], param_value)
        self._body += param_bytes

    @staticmethod
    def get_param_type(param_value: Union[int, float]):
        type_ = type(param_value)
        if type_ == float:
            return constants.DOUBLE_TYPE

        if type_ != int:
            raise SerializerUnhandledType("Type {} is not supported".format(type_))

        if param_value < 0:
            if -param_value <= constants.INT8_MAX:
                return constants.INT8_TYPE
            if -param_value <= constants.INT16_MAX:
                return constants.INT16_TYPE
            if -param_value <= constants.INT32_MAX:
                return constants.INT32_TYPE
            if -param_value <= constants.INT64_MAX:
                return constants.INT64_TYPE
            raise SerializerUnhandledType("Int {} is too big to bee serialized".format(param_value))
        else:
            if param_value <= constants.UINT8_MAX:
                return constants.UINT8_TYPE
            if param_value <= constants.UINT16_MAX:
                return constants.UINT16_TYPE
            if param_value <= constants.UINT32_MAX:
                return constants.UINT32_TYPE
            if param_value <= constants.UINT64_MAX:
                return constants.UINT64_TYPE
            raise SerializerUnhandledType("Int {} is too big to bee serialized".format(param_value))

    def make_message(self) -> bytes:
        message = struct.pack(constants.GROUP_INDEX_PACK_TYPE, self._group_index)
        message += struct.pack(constants.GROUP_SIZE_PACK_TYPE, len(self._body))
        message += self._body
        return message


class SerializerUnhandledType(Exception):
    pass
