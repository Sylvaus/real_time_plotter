import struct
from typing import Union, List, Tuple

from real_time_plotter.serializations import constants
from real_time_plotter.serializations.constants import PACK_TYPE

def get_group_index(header: bytes):
    return struct.unpack(constants.GROUP_INDEX_PACK_TYPE,
                         header[constants.GROUP_INDEX_OFFSET:
                                constants.GROUP_INDEX_OFFSET + constants.GROUP_INDEX_SIZE])[0]


def get_group_size(header: bytes):
    return struct.unpack(constants.GROUP_SIZE_PACK_TYPE,
                         header[constants.GROUP_SIZE_OFFSET:
                                constants.GROUP_SIZE_OFFSET + constants.GROUP_SIZE_SIZE])[0]


def get_param_index(param: bytes) -> int:
    return struct.unpack(constants.PARAM_INDEX_PACK_TYPE,
                         param[constants.PARAM_INDEX_OFFSET:
                               constants.PARAM_INDEX_OFFSET + constants.PARAM_INDEX_SIZE])[0]


def get_param_type(param: bytes) -> int:
    return struct.unpack(constants.PARAM_TYPE_PACK_TYPE,
                         param[constants.PARAM_TYPE_OFFSET:
                               constants.PARAM_TYPE_OFFSET + constants.PARAM_TYPE_SIZE])[0]


def get_param_size(param: bytes) -> int:
    param_type = get_param_type(param)
    return constants.PARAM_VAL_SIZES[param_type]


def deserialize_header(header: bytes):
    return get_group_index(header), get_group_size(header)


def deserialize_param(param: bytes) -> Tuple[int, int, Union[int, float]]:
    param_index = get_param_index(param)
    param_type = get_param_type(param)
    param_size = constants.PARAM_VAL_SIZES[param_type]

    param_val = struct.unpack(PACK_TYPE[param_type], param[constants.PARAM_HEADER_SIZE:
                                                           constants.PARAM_HEADER_SIZE + param_size])[0]

    return param_index, param_size, param_val


def deserialize_body(body: bytes) -> Tuple[List[int], List[Union[int, float]]]:
    parameter_indexes = []
    parameters = []
    offset = 0
    while offset < len(body):
        param_bytes = body[offset:]
        param_index, param_size, param_val = deserialize_param(param_bytes)
        parameter_indexes.append(param_index)
        parameters.append(param_val)

        offset += (param_size + constants.PARAM_HEADER_SIZE)

    return parameter_indexes, parameters
