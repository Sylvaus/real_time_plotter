from typing import List, Tuple, Union


class DataGroup:
    __slots__ = ["name", "param_names", "timestamp_params"]

    def __init__(self, name, param_names: List[str] = None,
                 timestamp_params: List[Tuple[float, List[Union[int, float]]]] = None):
        self.name = name
        if param_names is None:
            param_names = list()
        self.param_names = param_names
        if timestamp_params is None:
            timestamp_params = list()
        self.timestamp_params = timestamp_params

    def create_same_empty(self):
        return DataGroup(self.name, self.param_names)
