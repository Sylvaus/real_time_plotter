from abc import ABC, abstractmethod

from real_time_plotter.connectors.connector import ABConnector


class ABPlotter(ABC):
    def __init__(self, refresh_rate: int = 10):
        self._refresh_rate = refresh_rate
        self._connectors = []
        self._savers = []

    @abstractmethod
    def set_refresh_rate(self, refresh_rate: int):
        self._refresh_rate = refresh_rate

    @abstractmethod
    def add_connector(self, connector: ABConnector):
        self._connectors.append(connector)

    @abstractmethod
    def add_savers(self, saver):
        self._savers.append(saver)
