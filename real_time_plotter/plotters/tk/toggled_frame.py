import tkinter as tk
from tkinter import ttk


class ToggledFrame(tk.Frame):

    def __init__(self, parent, text="", *args, **options):
        tk.Frame.__init__(self, parent, *args, **options)

        self._show = tk.IntVar()
        self._show.set(0)

        self._header_frame = tk.Frame(self)
        self._header_frame.pack(fill="x", expand=1)

        self._title_frame = tk.Frame(self._header_frame)
        self._title_frame.pack(side="left", fill="x", expand=1)

        self._label = tk.Label(self._title_frame, text=text)
        self._label.pack(side="left", anchor="e")

        self._toggle_frame = tk.Frame(self._header_frame)
        self._toggle_frame.pack(side="left")

        self._toggle_button = ttk.Checkbutton(
            self._toggle_frame, width=2, text='+', command=self._toggle,
            variable=self._show, style='Toolbutton')
        self._toggle_button.pack(side="right")

        self._sub_frame = tk.Frame(self)

    @property
    def title_frame(self):
        return self._title_frame

    @property
    def sub_frame(self):
        return self._sub_frame

    def set_value(self, value: bool):
        if value != bool(self._show.get()):
            self._toggle()

    def _toggle(self):
        if bool(self._show.get()):
            self._sub_frame.pack(fill="x", expand=1)
            self._toggle_button.configure(text='-')
        else:
            self._sub_frame.forget()
            self._toggle_button.configure(text='+')