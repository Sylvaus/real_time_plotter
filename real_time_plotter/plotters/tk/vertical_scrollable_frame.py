"""
Vertical Scrollable Frame based on https://stackoverflow.com/questions/16188420/python-tkinter-scrollbar-for-frame
"""

import tkinter as tk


class VerticalScrolledFrame(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)

        # Create a canvas object and a vertical scrollbar for scrolling it
        self._vscrollbar = tk.Scrollbar(self, orient=tk.VERTICAL)
        self._vscrollbar.pack(fill=tk.Y, side=tk.RIGHT, expand=tk.FALSE)
        self._canvas = tk.Canvas(self, bd=0, highlightthickness=0,  yscrollcommand=self._vscrollbar.set)
        self._canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.TRUE)
        self._vscrollbar.config(command=self._canvas.yview)

        # Reset the view
        self._canvas.xview_moveto(0)
        self._canvas.yview_moveto(0)

        # Create a frame inside the canvas which will be scrolled with it
        self._interior = interior = tk.Frame(self._canvas)
        self._interior_id = self._canvas.create_window(0, 0, window=interior, anchor=tk.NW)

        # Track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        self._interior.bind('<Configure>', self._configure_interior)
        self._canvas.bind('<Configure>', self._configure_canvas)

        # Handling scrolling with mousewheel
        self._canvas.bind_all("<MouseWheel>", self._on_mousewheel)

    @property
    def interior(self):
        return self._interior

    def _configure_interior(self, event):
        # update the scrollbars to match the size of the inner frame
        scroll_region = "0 0 {} {}".format(
            self._interior.winfo_reqwidth(),
            self._interior.winfo_reqheight()
        )
        self._canvas.config(scrollregion=scroll_region)
        if self._interior.winfo_reqwidth() != self._canvas.winfo_width():
            # update the canvas's width to fit the inner frame
            self._canvas.config(width=self._interior.winfo_reqwidth())

    def _configure_canvas(self, event):
        if self._interior.winfo_reqwidth() != self._canvas.winfo_width():
            # update the inner frame's width to fill the canvas
            self._canvas.itemconfigure(self._interior_id, width=self._canvas.winfo_width())

    def _on_mousewheel(self, event):
        increment = int(-(event.delta/120))
        # Check put in place to avoid having the content moving
        # when all the elements are displayed
        if (0.0, 1.0) != self._vscrollbar.get():
            self._canvas.yview_scroll(increment, "units")
