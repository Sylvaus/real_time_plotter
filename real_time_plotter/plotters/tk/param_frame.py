import tkinter as tk
from typing import Callable


class ParamFrame(tk.Frame):
    UNCHECKED = 0
    CHECKED = 1

    def __init__(self, parent, name: str,
                 notify_change_func: Callable[[None], None], **kwargs):
        super().__init__(parent, **kwargs)

        self._name = name
        self._state = tk.IntVar()
        self._label = tk.Label(self, text=name)
        self._label.pack(side="left")
        self._button = tk.Checkbutton(
            self, text="", variable=self._state,
            onvalue=self.CHECKED, offvalue=self.UNCHECKED, command=notify_change_func)
        self._button.pack(side="right")

    @property
    def name(self) -> str:
        return self._name

    def is_checked(self) -> bool:
        return self._state.get() == self.CHECKED

    def set_checked(self, checked: bool):
        self._state.set(self.CHECKED if checked else self.UNCHECKED)


