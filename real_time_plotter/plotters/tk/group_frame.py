from typing import Callable, List

from real_time_plotter.plotters.tk.param_frame import ParamFrame
from real_time_plotter.plotters.tk.toggled_frame import ToggledFrame


class GroupFrame(ToggledFrame):
    def __init__(self, parent, name: str,
                 notify_change_func: Callable[[None], None],  *args, **options):
        super().__init__(parent, name, *args, **options)

        self._name = name
        self._notify_change = notify_change_func
        self._params = {}

    @property
    def name(self):
        return self._name

    @property
    def param_names(self) -> List[str]:
        return list(self._params.keys())

    @property
    def checked_params(self):
        return [param.name for param in self._params if param.is_checked]

    def add_param(self, name: str):
        param_frame = ParamFrame(self._sub_frame, name, self._notify_change)
        param_frame.pack(side="bottom", fill="x", expand=1, padx=(10, 0))
        self._params[name] = param_frame

    def remove_param(self, name: str):
        if name in self._params:
            param = self._params.pop(name)
            param.destroy()

    def set_param(self, name: str, checked: bool):
        if name not in self._params:
            return

        self._params[name].set_checked(checked)







