from typing import List, Dict

from real_time_plotter.plotters.tk.group_frame import GroupFrame
from real_time_plotter.plotters.tk.toggled_frame import ToggledFrame


class PlotFrame(ToggledFrame):
    def __init__(self, parent, name: str, *args, **options):
        super().__init__(parent, name, width=500, height=50, *args, **options)
        self._groups = {}
        self._has_changed = False

    def add_group(self, name, param_names: List[str]):
        group_frame = GroupFrame(self._sub_frame, name, self._notify_has_changed)

        for name in param_names:
            group_frame.add_param(name)
        group_frame.pack(side="bottom", fill="x", expand=1, padx=(10, 0))

        self._groups[name] = group_frame

    def update_group(self, name: str, param_names: List[str]):
        if name not in self._groups:
            return
        group = self._groups[name]

        current = set(group.param_names)
        expected = set(param_names)

        to_remove = current - expected
        for param in to_remove:
            group.remove_param(param)

        to_add = expected - current
        for param in to_add:
            group.add_param(param)

    def remove_group(self, name: str):
        if name in self._groups:
            group = self._groups[name]
            group.destroy()

    def has_changed(self) -> bool:
        result = self._has_changed
        self._has_changed = False
        return result

    def check_params(self) -> Dict[str, List[str]]:
        return {
            name: group.checked_params
            for name, group in self._groups
            if len(group.checked_params) > 0
        }

    def set_param_state(self, group_name: str, param_name: str, checked: bool):
        if group_name not in self._groups:
            return

        self._groups[group_name].set_param(param_name, checked)

    def _notify_has_changed(self):
        self._has_changed = True
