import tkinter as tk
from typing import List

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
# Implement the default Matplotlib key bindings.
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure

import numpy as np

from real_time_plotter.connectors.connector import ABConnector
from real_time_plotter.plotters.plotter import ABPlotter
from real_time_plotter.plotters.tk.plot_frame import PlotFrame
from real_time_plotter.plotters.tk.vertical_scrollable_frame import VerticalScrolledFrame


class PlotPanel(tk.Frame):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._plots = {}
        self._groups = {}
        self._plot_frame = VerticalScrolledFrame(self, relief="sunken", borderwidth=1)
        self._plot_frame.pack(side="top",  fill="x", expand=1)

        self._buttons_frame = tk.Frame(self)
        self._buttons_frame.pack(side="top", fill=None)

        self._add_button = tk.Button(self._buttons_frame, text="Add Plot", command=self.add_plot)
        self._add_button.pack(side="left", fill=None)
        self._remove_button = tk.Button(self._buttons_frame, text="Remove Plot", command=self.remove_plot)
        self._remove_button.pack(side="left", fill=None)

    def add_plot(self):
        name_plot = "Plot {}".format(len(self._plots))
        plot = PlotFrame(self._plot_frame.interior, name_plot)
        plot.pack(side="top", fill="x", expand=1)
        self._plots[name_plot] = plot

    def remove_plot(self, plot_name: str = None):
        if len(self._plots) == 0:
            return

        if plot_name is None:
            _, plot = self._plots.popitem()
        elif plot_name in self._plots:
            _, plot = self._plots.pop(plot_name)
        else:
            return

        plot.destroy()

    def add_group(self, name: str, param_names: List[str]):
        self._groups[name] = param_names
        for plot in self._plots:
            plot.add_group(name, param_names)

    def update_group(self, name: str, param_names: List[str]):
        self._groups[name] = param_names
        for plot in self._plots.values():
            plot.update_group(name, param_names)

    def remove_group(self, name: str):
        if name not in self._groups:
            return

        for plot in self._plots.values():
            plot.remove_group(name)


class TkMatplotlibPlotter(ABPlotter, tk.Frame):
    def __init__(self, master=None, *args, **kwargs):
        if master is None:
            self._master = tk.Tk()
            tk.Frame.__init__(self, master, *args, **kwargs)
            self.pack()
        else:
            self._master = master
            tk.Frame.__init__(self, master)

        fig = Figure(figsize=(5, 4), dpi=100)
        t = np.arange(0, 3, .01)
        fig.add_subplot(111).plot(t, 2 * np.sin(2 * np.pi * t))

        canvas = FigureCanvasTkAgg(fig, master=self)  # A tk.DrawingArea.
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        toolbar = NavigationToolbar2Tk(canvas, self)
        toolbar.update()
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        def on_key_press(event):
            print("you pressed {}".format(event.key))
            key_press_handler(event, canvas, toolbar)

        canvas.mpl_connect("key_press_event", on_key_press)

        def _quit():
            self._master.quit()  # stops mainloop
            self._master.destroy()  # this is necessary on Windows to prevent
            # Fatal Python Error: PyEval_RestoreThread: NULL tstate

        button = tk.Button(master=self._master, text="Quit", command=_quit)
        button.pack(side=tk.BOTTOM)

    def set_refresh_rate(self, refresh_rate: int):
        pass

    def add_connector(self, connector: ABConnector):
        pass

    def add_savers(self, saver):
        pass


def main():
    root = tk.Tk()
    # plot = PlotFrame(root, "plot 1")
    # plot.pack(fill="x", expand=1)
    # plot.add_group("group_1", ["param_1"])
    PlotPanel(root).pack()

    root.mainloop()


if __name__ == '__main__':
    main()


