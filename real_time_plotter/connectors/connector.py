"""
This module provides the interface for the connectors
A connector is an object that provides the data to be plotted
"""

from abc import ABC, abstractmethod
from typing import Dict, List, Tuple, Union

from real_time_plotter.data.group import DataGroup


class ABConnector(ABC):
    def __init__(self, queue_size: int):
        self._queue_size = queue_size

    def set_queue_size(self, queue_size: int):
        self._queue_size = queue_size

    @abstractmethod
    def connect(self) -> bool:
        pass

    @abstractmethod
    def disconnect(self):
        pass

    @abstractmethod
    def get_data(self) -> Dict[str, DataGroup]:
        pass
