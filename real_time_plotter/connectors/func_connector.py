import time
from typing import Dict
from multiprocessing import Lock
from threading import Thread

from real_time_plotter.connectors.connector import ABConnector
from real_time_plotter.data.group import DataGroup


class FuncConnector(ABConnector):
    def __init__(self, group_name_param_names, group_name_funcs, period: float, queue_size: int):
        super().__init__(queue_size)
        self._group_name_funcs = group_name_funcs
        self._period = period
        self._run = False
        self._update_thread = None
        self._data_get_lock = Lock()
        self._data = {}
        for group_name, param_names in group_name_param_names:
            group = DataGroup(group_name)
            group.param_names = param_names
            self._data[group_name] = group

    def get_data(self) -> Dict[str, DataGroup]:
        self._data_get_lock.acquire()
        result = self._data
        self._data = dict()
        for name, group in result:
            self._data[name] = group.create_same_empty()
        self._data_get_lock.release()
        return result

    def connect(self) -> bool:
        self._run = True
        self._update_thread = Thread(target=self._update_data)
        self._update_thread.start()
        return True

    def disconnect(self):
        self._run = False
        return True

    def _update_data(self):
        while self._run:
            timestamp = time.time()
            self._data_get_lock.acquire()
            for group_name, func in self._group_name_funcs.iteritems():
                group = self._data[group_name]
                group.timestamp_params.append((timestamp, func(timestamp)))
                if len(group.timestamp_params):
                    group.timestamp_params.pop(0)
            self._data_get_lock.release()

            wait_time = self._period - (time.time() - timestamp)
            if wait_time > 0:
                time.sleep(wait_time)
