from abc import abstractmethod
from threading import Lock
from typing import Dict, List, Tuple, Union

from real_time_plotter.connectors.connector import ABConnector
from real_time_plotter.data.group import DataGroup
from real_time_plotter.serializations.deserializer import get_group_index, deserialize_body


class ABDefaultSerializationConnector(ABConnector):
    def __init__(self, queue_size: int,
                 index_group_name: Dict[int, str] = None,
                 index_param_name: Dict[int, str] = None):
        super().__init__(queue_size)
        self._queue_size = queue_size
        if index_group_name is None:
            index_group_name = dict()
        self._index_group_name = index_group_name
        if index_param_name is None:
            index_param_name = dict()
        self._index_param_name = index_param_name
        self._data = dict()
        self._data_get_lock = Lock()

    def set_index_group_name(self, index_group_name: Dict[int, str]):
        self._index_group_name = index_group_name

    def set_index_param_name(self, index_param_name: Dict[int, str]):
        self._index_param_name = index_param_name

    def set_queue_size(self, queue_size: int):
        self._queue_size = queue_size

    def _update_data(self, header: bytes, body: bytes, timestamp: float):
        group_index = get_group_index(header)
        parameter_indexes, parameters = deserialize_body(body)
        if group_index not in self._index_group_name:
            self._index_group_name[group_index] = "group_{}".format(group_index)
            for index in parameter_indexes:
                if index not in self._index_param_name:
                    self._index_param_name[index] = "param_{}".format(index)

        group_name = self._index_group_name[group_index]
        self._data_get_lock.acquire()
        if group_name not in self._data:
            data_group = DataGroup(group_name)
            self._data[group_name] = data_group
            for index in parameter_indexes:
                param_name = self._index_param_name[index]
                data_group.param_names.append(param_name)
        else:
            data_group = self._data[group_name]

        data_group.timestamp_params.append(timestamp, parameters)
        if len(data_group.timestamp_params):
            data_group.timestamp_params.pop(0)
        self._data_get_lock.release()

    @abstractmethod
    def connect(self) -> bool:
        pass

    @abstractmethod
    def disconnect(self):
        pass

    def get_data(self) -> Dict[str, DataGroup]:
        self._data_get_lock.acquire()
        result = self._data
        self._data = dict()
        for name, group in result:
            self._data[name] = group.create_same_empty()
        self._data_get_lock.release()
        return result

