"""
Module providing the SerialConnector class to receive info from serial port
"""
import time
from collections import defaultdict
from typing import Dict
from multiprocessing import Lock
from threading import Thread

import serial

from real_time_plotter.connectors.default_serialization_connector import ABDefaultSerializationConnector
from real_time_plotter.serializations import constants
from real_time_plotter.serializations.deserializer import get_group_size


class SerialConnector(ABDefaultSerializationConnector):
    def __init__(self, com_port: str, baudrate: int, queue_size: int,
                 index_group_name: Dict[int, str] = None,
                 index_param_name: Dict[int, str] = None,
                 **serial_opts):
        super().__init__(queue_size, index_group_name, index_param_name)
        self._com_port = com_port
        self._baudrate = baudrate
        self._serial_opts = serial_opts
        self._serial_port = None
        self._receive = False
        self._receiver_thread = None
        self._connected = False

    def connect(self) -> bool:
        if self._connected:
            return True

        try:
            self._serial_port = serial.Serial(self._com_port)
        except serial.SerialException:
            return False

        self._receive = True
        self._receiver_thread = Thread(target=self._receiver)
        self._receiver_thread.start()
        self._connected = True

        return True

    def _receiver(self):
        while self._receive:
            header_bytes = self._serial_port.read(constants.HEADER_SIZE)
            timestamp = time.time()
            body_bytes = self._serial_port.read(get_group_size(header_bytes))
            self._update_data(header_bytes, body_bytes, timestamp)

    def disconnect(self):
        self._receive = False

        if self._serial_port and self._serial_port.is_open():
            self._serial_port.close()

        self._connected = False


if __name__ == '__main__':
    connector = SerialConnector("com0", 19200)
    connector.connect()
