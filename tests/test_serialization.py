from unittest import TestCase

from real_time_plotter.serializations.constants import HEADER_SIZE, INT8_TYPE, INT16_TYPE, INT32_TYPE, INT64_TYPE, \
    UINT8_TYPE, UINT16_TYPE, UINT32_TYPE, UINT64_TYPE
from real_time_plotter.serializations.deserializer import deserialize_header, deserialize_body
from real_time_plotter.serializations.serializer import Serializer, SerializerUnhandledType


class TestSerialization(TestCase):
    def test_serialization_int8(self):
        min_int = -127
        min_index = 0xFA
        auto_type_int = -54
        auto_type_index = 0x1FA
        max_int = 127
        max_index = 0x51FA
        group_index = 0x12FA
        type_ = INT8_TYPE

        self.serialization_type(group_index,
                                max_index, max_int,
                                min_index, min_int,
                                auto_type_index, auto_type_int, type_)

    def test_serialization_int16(self):
        min_int = -32767
        min_index = 0xEA
        auto_type_int = -3276
        auto_type_index = 0x1EA
        max_int = 32767
        max_index = 0x51EA
        group_index = 0x12FA
        type_ = INT16_TYPE

        self.serialization_type(group_index,
                                max_index, max_int,
                                min_index, min_int,
                                auto_type_index, auto_type_int, type_)

    def test_serialization_int32(self):
        min_int = -2147483647
        min_index = 0xFA
        auto_type_int = -247483647
        auto_type_index = 0x1FA
        max_int = 2147483647
        max_index = 0x51FA
        group_index = 0x12FA
        type_ = INT32_TYPE

        self.serialization_type(group_index,
                                max_index, max_int,
                                min_index, min_int,
                                auto_type_index, auto_type_int, type_)

    def test_serialization_int64(self):
        min_int = -9223372036854775807
        min_index = 0xFA
        auto_type_int = -23372036854775807
        auto_type_index = 0x1FA
        max_int = 9223372036854775807
        max_index = 0x51FA
        group_index = 0x12FA
        type_ = INT64_TYPE

        self.serialization_type(group_index,
                                max_index, max_int,
                                min_index, min_int,
                                auto_type_index, auto_type_int, type_)

    def test_serialization_uint8(self):
        min_int = 0
        min_index = 0xFA
        auto_type_int = 55
        auto_type_index = 0x1FA
        max_int = 255
        max_index = 0x51FA
        group_index = 0x12FA
        type_ = UINT8_TYPE

        self.serialization_type(group_index,
                                max_index, max_int,
                                min_index, min_int,
                                auto_type_index, auto_type_int, type_)

    def test_serialization_uint16(self):
        min_int = 0
        min_index = 0xEA
        auto_type_int = 32900
        auto_type_index = 0x1EA
        max_int = 65535
        max_index = 0x51EA
        group_index = 0x12FA
        type_ = UINT16_TYPE

        self.serialization_type(group_index,
                                max_index, max_int,
                                min_index, min_int,
                                auto_type_index, auto_type_int, type_)

    def test_serialization_uint32(self):
        min_int = 0
        min_index = 0xFA
        auto_type_int = 3294967295
        auto_type_index = 0x1FA
        max_int = 4294967295
        max_index = 0x51FA
        group_index = 0x12FA
        type_ = UINT32_TYPE

        self.serialization_type(group_index,
                                max_index, max_int,
                                min_index, min_int,
                                auto_type_index, auto_type_int, type_)

    def test_serialization_uint64(self):
        min_int = 0
        min_index = 0xFA
        auto_type_int = 16446744073709551615
        auto_type_index = 0x1FA
        max_int = 18446744073709551615
        max_index = 0x51FA
        group_index = 0x12FA
        type_ = UINT64_TYPE

        self.serialization_type(group_index,
                                max_index, max_int,
                                min_index, min_int,
                                auto_type_index, auto_type_int, type_)

    def test_unsupported_type(self):
        group_index = 0x12FA
        serializer = Serializer(group_index)
        self.assertRaises(SerializerUnhandledType, serializer.add_parameter, 0x1, "hi")

    def test_unsupported_type_too_big_nb(self):
        group_index = 0x12FA
        serializer = Serializer(group_index)
        self.assertRaises(SerializerUnhandledType, serializer.add_parameter, 0x1, 18446744073709551615 * 3)

    def serialization_type(self, group_index,
                           max_index, max_int,
                           min_index, min_int,
                           auto_type_index, auto_type_int, type_):
        serializer = Serializer(group_index)
        serializer.add_parameter(min_index, min_int, type_)
        serializer.add_parameter(auto_type_index, auto_type_int)
        serializer.add_parameter(max_index, max_int, type_)
        message = serializer.make_message()
        received_group_index, _ = deserialize_header(message)
        self.assertEqual(received_group_index, group_index)
        parameter_index, parameters = deserialize_body(message[HEADER_SIZE:])
        self.assertEqual(min_int, parameters[0])
        self.assertEqual(min_index, parameter_index[0])
        self.assertEqual(auto_type_int, parameters[1])
        self.assertEqual(auto_type_index, parameter_index[1])
        self.assertEqual(max_int, parameters[2])
        self.assertEqual(max_index, parameter_index[2])
